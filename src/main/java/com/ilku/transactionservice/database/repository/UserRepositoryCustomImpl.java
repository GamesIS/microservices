package com.ilku.transactionservice.database.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepositoryCustomImpl {

    public final MongoTemplate mongoTemplate;
    @Autowired//TODO Ее необязательно писать?
    public UserRepositoryCustomImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }
}
