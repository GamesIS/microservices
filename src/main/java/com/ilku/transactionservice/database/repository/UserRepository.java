package com.ilku.transactionservice.database.repository;

import com.ilku.transactionservice.database.document.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.math.BigInteger;
import java.util.List;

public interface UserRepository  extends MongoRepository<User, BigInteger> {
    List<User> findByFirstNameLike(String fullName);

    List<User> findByNickGreaterThan(String nick);
}
