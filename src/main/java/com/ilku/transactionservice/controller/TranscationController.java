package com.ilku.transactionservice.controller;

import com.ilku.transactionservice.database.document.User;
import com.ilku.transactionservice.database.repository.UserRepository;
import com.ilku.transactionservice.database.repository.UserRepositoryCustomImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigInteger;

@RequestMapping("/api/transaction")
@RestController
@Api("Тест TransactionController")
public class TranscationController {

    public final UserRepositoryCustomImpl userRepositoryCustom;
    public final UserRepository userRepository;

    @Autowired
    public TranscationController(UserRepositoryCustomImpl userRepositoryCustom, UserRepository userRepository) {
        this.userRepositoryCustom = userRepositoryCustom;
        this.userRepository = userRepository;
    }

    @ApiOperation("Test")
    @GetMapping("/adduser")
    public User addUser(){
        User user = User.builder()
                .firstName("Ilya")
                .lastName("Kuznetsov")
                .nick("GamesIS")
                .build();
        userRepository.insert(user);
        return user;
    }

}
